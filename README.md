# mutantC-v2

# Checkout the poject website from [here](https://mutantc.gitlab.io).
### A Raspberry-pi handheld platform with a physical keyboard, Display and Expansion header for custom baords (Like Ardinuo Shield).
### It is fully open-source hardware with MIT. So you can modify or hack it as you wish.
### It is platform like Arduino. You can make your expansion-card like gps, Radio etc.
### So help us to make a Community around it.
### [Parts list](https://gitlab.com/mutantC/mutantc-v2/blob/master/parts_list).

# Made by rahmanshaber( @rahmanshaber:matrix.org )

## mutantC v3 PCB can be used, but needs case modmodifications modifications.

# Changes
*  Power  on/off switch .
*  18650 battery with charge and discharge protaction .
*  Pi 4 support .
*  Stop position for sliding  mechanism.
*  Keyboard layout change led.
*  Adafruit lcd support.
*  5 column keyboard .
*  2 shoulder button.
*  Reduce keyboard power usage .
*  Reduced width from 39mm to 35mm.

# Download
### Download the project from [here](https://a360.co/2wWp7yi).

# Software used
### Autodesk fusion 360 - 3d parts
### Eagle - PCB
### Ardinuo IDE - Device Firmware

<img src="p1.png" width="500"> 
<img src="p2.png" width="500"> 
<img src="p6.png" width="500"> 
<img src="p3.png" width="500">
<img src="p7.png" width="500">
<img src="p4.png" width="500"> 
<img src="p5.png" width="500"> 
<img src="p8.png" width="500"> 
<img src="p9.png" width="500">

# Feedback
We need your feedback to improve the mutantC.
Send us your feedback through GitLab [issues](https://gitlab.com/groups/mutantC/-/issues).
